import argparse
from multiprocessing import Pool, cpu_count
import sys

def get_args():
    '''Gets the CLI args that were passed into the script.'''

    about = '''Takes a intger or range of integers and determins the 
               multiplication persistence. If a range is given the first
               highest persistence value is returned '''
    
    parser = argparse.ArgumentParser(description=about)
    group = parser.add_mutually_exclusive_group()
    group.add_argument('-n', 
                       '--number',
                       type=int,
                       help="Integer",
                       )
    group.add_argument('-r', 
                       '--range',
                       help="Integer range. Example: 20-77",
                       )

    parser.add_argument('-v', 
                        '--verbose',
                        action='store_true',
                        )
    args = parser.parse_args()

    if args.number is not None:
        return (args.number, args.verbose)
    else:
        return (args.range, args.verbose)

def per(number, verbose=False):
    '''
    This will take a number split the number into a list of single digits then
    multiply each digit together. Returns a tuple with the steps and number.
    '''
    num1 = number
    steps = 1
    while (True):
        result = 1
        if verbose == True:
            print ('Number: ', num1)
            print ('Step: ', steps)

        digit_list = [int(digit) for digit in str(num1)]
        for digit in digit_list:
            result *= digit

        num1 = result
        if len(str(num1)) == 1:
            return (steps, number)
        steps += 1



if __name__ == '__main__':
    number, verbose = get_args()
    if '-' in str(number):
        range1 = number.split('-')
        start = int(range1[0])
        stop = int(range1[-1]) + 1
        highest = 0
        step_count = 0
        cores = cpu_count() * 2
        with Pool() as pool:
            result = [x for x in pool.imap(per, range(start,stop),int((stop-start)/cores))]
        for steps, num in result:
            if steps > step_count:
                step_count = steps
                highest = num
        else:
             print ('Highest number: ', highest)
             print ('Total Steps: ', step_count)
             sys.exit(0)
            
    else:
        steps, num1 = per(number, verbose)
        print ('Total Steps: ', steps)
        print ('Number: ', num1)


